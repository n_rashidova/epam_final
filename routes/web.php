<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Client\ApplicationController as ClientApplicationController;
use App\Http\Controllers\Brigade\BrigadeController as BrigadePageController;
use App\Http\Controllers\Operator\ApplicationController as OperatorApplicationController;
use App\Http\Controllers\Operator\BrigadeController as OperatorBrigadeController;
use App\Http\Controllers\Operator\OperatorController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::prefix('client')->name('client.')->group(function (){

    Route::resource('applications', ClientApplicationController::class)
        ->only(['index', 'create', 'store']);

    Route::get('applications/{user}', [ClientApplicationController::class, 'showClosedApplications'])
        ->name('applications.showClosedApplications');

    Route::put('applications/{application}/set/done', [ClientApplicationController::class, 'setDoneByClient'])
        ->name('applications.setDoneByClient');
});



Route::prefix('operator')->name('operator.')->group(function (){
    Route::resource('applications', OperatorApplicationController::class)->only(['index', 'show']);

    Route::get('applications/{user}/closed/applications',
        [OperatorApplicationController::class, 'closedApplications'])->name('applications.closedApplications');

    Route::get('applications/{user}/applications', [OperatorApplicationController::class, 'operatorApplications'])
        ->name('applications.operatorApplications');


    Route::put('applications/{application}/set', [OperatorApplicationController::class, 'setOperator'])
        ->name('applications.setOperator');


    Route::get('applications/{application}/choose/brigade',
        [OperatorApplicationController::class, 'chooseBrigade'])->name('applications.chooseBrigade');

    Route::put('applications/{application}/brigades/{brigade}',
        [OperatorApplicationController::class, 'setBrigade'])->name('applications.setBrigade');

    Route::put('applications/{application}/set/done',
        [OperatorApplicationController::class, 'setDoneByOperator'])->name('applications.setDoneByOperator');





    Route::get('brigades', [OperatorBrigadeController::class, 'index'])
        ->name('brigades.index');

    Route::resource('brigades', OperatorBrigadeController::class)
        ->except([ 'show']);

    Route::resource('users', OperatorController::class)->except('show');

    Route::get('users/brigades/members', [OperatorController::class, 'showBrigadeMembers'])
        ->name('brigades.showBrigadeMembers');
});




Route::get('brigades/{brigade}', [BrigadePageController::class, 'show'])
    ->name('brigade.show');

Route::prefix('brigade')->name('brigade.')->group(function (){
    Route::put('brigades/{application}/set/process', [BrigadePageController::class, 'setInProcess'])
        ->name('setInProcess');

    Route::put('brigades/{application}/set/done', [BrigadePageController::class, 'setDone'])
        ->name('setDone');
});





