@extends('layouts.client')
@section('content')
    <div class="main-block">
        <div class="container">
            <div class="row row-cols-sm-1 row-cols-md-2 row-cols-lg-2 justify-content-center">
                <div class="col">
                    <img class=" mt-5" src="{{ asset('pictures/trash.jpg') }}" alt="pic-big-1">
                </div>
                <div class="col main-block-col pl-4">
                    <h2 class="text-uppercase mt-5 mb-4">Помогаем бизнесу <span class="emphasize">эффективно</span> управлять отходами</h2>
                    <p>Утилизация вторсырья осуществляется в соответствии с утвержденными законодательством нормами. По запросу мы готовы предоставить соответствующий документ, подтверждающий безопасность выполненных мероприятий.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="how-we-work-block mt-5 mb-5 pt-5 pb-5">
        <div class="container">
            <div class="row row-cols-sm-1 row-cols-md-2 row-cols-lg-2 justify-content-center ">
                <div class="col how-we-work-col">
                    <h2 class="text-uppercase mb-4">Услуги предоставляемые компанией <span class="emphasize">EPAM</span></h2>
                    <ul class="ul-pic">
                        <li>Наша компания производит экобоксы для раздельного сбора мусора</li>
                        <li>Устанавливает экобоксы</li>
                        <li>Предоставляет услуги по вывозу мусора и дальнейшей его переработки</li>
                        <li>При покупке экобоксов, услуги по их установке и вывозу мусора бесплатны</li>
                    </ul>
                </div>
                <div class="col">
                    <img src="{{ asset('pictures/recyclee.jpg') }}" alt="pic-big-2">
                </div>
            </div>
        </div>
    </div>

    <div class="ecobox-type-board mt-5">
        <div class="container">
            <h2 class="text-uppercase text-center mb-5 mt-5">Производим экобоксы для: </h2>
            <div class="row row-cols-sm-2 row-cols-md-3 row-cols-lg-4 mb-4">
                <div class="col">
                    <div class="card">
                        <img src="{{ asset('pictures/paper-recycle.jpg') }}" class="card-img-top" alt="paper">
                        <div class="card-body text-center">
                            <a href="#"><button class="card-btn card-btn-1 btn-primary text-center mb-3" type="submit">Graphics</button></a>
                            <p class="card-text">Макулатура</p>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                        <img src="{{ asset('pictures/plastic-recycle.jpg') }}" class="card-img-top" alt="pic-2">
                        <div class="card-body text-center">
                            <a href="#"><button class="card-btn btn-success card-btn-2 text-center mb-3" type="submit">Graphics</button></a>
                            <p class="card-text">Твердый пластик</p>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card" >
                        <img src="{{ asset('pictures/organic-recycle.jpg') }}" class="card-img-top" alt="pic-3">
                        <div class="card-body text-center">
                            <a href="#"><button class="card-btn card-btn-3 text-center mb-3" type="submit">Graphics</button></a>
                            <p class="card-text"> Пищевые отходы</p>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card" >
                        <img src="{{ asset('pictures/plenka-recycle.jpg') }}" class="card-img-top" alt="pic-4">
                        <div class="card-body text-center">
                            <a href="#"><button class="card-btn btn-warning card-btn-4 text-center mb-3" type="submit">Graphics</button></a>
                            <p class="card-text">Полиэтиленовая пленка</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <footer class="footer mt-5">
        <div class="container">
            <p>© 2023 EPAM </p>
        </div>
    </footer>


@endsection
