@extends('layouts.operator')

@section('content')
    <div class="container mt-4">

        <div class="row mt-4">
            <form action="{{route('operator.brigades.store')}}" method="post">
                @csrf

                <div class="row mb-3">
                    <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Название бригады') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="members" class="col-md-4 col-form-label text-md-end">{{ __('Члены бригады') }}</label>
                    <div class="col-md-6">
                        <select name="user_id[]" class="form-select @error('name') is-invalid @enderror " multiple aria-label="multiple select example">
                            <option selected>Выберите члена бригады</option>
                            @foreach($brigade_members as $member)
                                <option value="{{$member->id}}">{{$member->name}}</option>
                            @endforeach
                        </select>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-success press-btn">
                            Создать бригаду
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection

