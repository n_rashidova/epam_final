@extends('layouts.operator')
@section('content')
    <div class="container mt-4">
        @include('notifications.alerts')
        <div class="row links mb-5">
            <div >
                <a href="{{ route('operator.users.create') }}">Добавить специалиста по технической поддержке</a>
            </div>
        </div>
        <div class="row">
            <h3>Бригада</h3>
        </div>
        <div class="mt-4 mb-5">
            <table class="table">
                <thead>
                <tr>
                    <th>Имя</th>
                    <th>Email</th>
                    <th>Изменить</th>
                    <th>Удалить</th>
                </tr>
                </thead>
                <tbody>
                @foreach($brigade_members as $user)
                    <tr>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                            <a class="btn btn-sm btn-outline-success press-btn" href="{{route('operator.users.edit', ['user' => $user])}}">Изменить</a>
                        </td>
                        <td>
                            <form action="{{route('operator.users.destroy', ['user' => $user])}}" method="post">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-sm btn-danger">Удалить</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
