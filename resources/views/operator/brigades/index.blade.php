@extends('layouts.operator')
@section('content')
    <div class="container mt-4">
        @include('notifications.alerts')

        <div class="row links mb-5">
            <div class="col-sm-2">
                <a href="{{ route('operator.brigades.create') }}">Создать бригаду</a>
            </div>
            <div class="col-sm-2">
                <a href="{{ route('operator.brigades.showBrigadeMembers') }}">Все члены бригад</a>
            </div>
        </div>

        <div class="row">
            <h3>Бригады</h3>
        </div>

        <div class="mt-4 mb-5">
            <table class="table">
                <thead>
                <tr>
                    <th>Название</th>
                    <th>Статус</th>
                    <th>Изменить</th>
                    <th>Удалить</th>
                </tr>
                </thead>
                <tbody>
                @foreach($brigades as $brigade)
                    <tr>
                        <td>{{$brigade->name}}</td>
                        <td>{{$brigade->status}}</td>
                        <td>
                            <a class="btn btn-sm btn-success press-btn" href="{{ route('operator.brigades.edit', ['brigade' => $brigade]) }}">Изменить</a>
                        </td>
                        <td>
                            <form action="{{ route('operator.brigades.destroy', ['brigade' => $brigade]) }}" method="post">
                                @method('delete')
                                @csrf
                                <button class="btn btn-sm btn-danger delete-btn" type="submit">Удалить</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection


