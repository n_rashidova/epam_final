@extends('layouts.operator')
@section('content')
    <div class="container mt-4">

        <div class="row links mb-5">
            <div class="col-sm-2">
                <a href="{{ route('operator.applications.index')}}">
                    <p>Все заявки</p>
                </a>
            </div>
            <div class="col-sm-2">
                <a href="{{ route('operator.applications.operatorApplications', ['user' => $user]) }}">
                    <p>Мои заявки</p>
                </a>
            </div>

            <div class="col-sm-2">
                <a href="{{ route('operator.applications.closedApplications', ['user' => $user]) }}">
                    <p>Закрытые заявки /</p>
                </a>
            </div>
        </div>

        <h3>Подробнее о заявке</h3>

        <table class="table table-hover mt-5 mb-5">
            <thead>
            <tr>
                <th>Заявка</th>
                <th>Комментарий</th>
                <th>Дата открытия</th>
                <th>Статус</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $application->type->name }}</td>
                    <td>{{ $application->comment }}</td>
                    <td>{{ $application->created_at }}</td>
                    <td>{{ $application->getStatusForOperator()}}</td>
                </tr>
            </tbody>
        </table>

        <h5 class="headline mb-3">Информация о клиенте:</h5>
        <div class="row client-info-block">
            <div class="col-12 col-lg-6 ">
                <ul>
                    <li><strong>Имя:  </strong> {{$application->user->name}}</li>
                    <li><strong>Номер телефона:  </strong>{{$application->user->phone_number}}</li>
                    <li><strong>Адрес:  </strong>{{$application->user->address}}</li>
                    <li><strong>Почта:  </strong>{{$application->user->email}}</li>
                </ul>
            </div>
        </div>
    </div>
@endsection
