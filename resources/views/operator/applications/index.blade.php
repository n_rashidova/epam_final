@extends('layouts.operator')

@section('content')

    <div class="container mt-4">
        @include('notifications.alerts')

        <div class="row links mb-5">
            <div class="col-sm-2">
                <a href="{{ route('operator.applications.index')}}">
                    <p>Все заявки /</p>
                </a>
            </div>
            <div class="col-sm-2">
                <a href="{{ route('operator.applications.operatorApplications', ['user' => $user]) }}">
                    <p>Мои заявки /</p>
                </a>
            </div>

            <div class="col-sm-2">
                <a href="{{ route('operator.applications.closedApplications', ['user' => $user]) }}">
                    <p>Закрытые заявки /</p>
                </a>
            </div>
        </div>
        @if(count($active_applications))
                <div class="row mt-5">
                    <div class="col">
                        <h3>Все новые заявки</h3>
                    </div>
                </div>
                <div class="row all-appl-block mt-4">
                    <div class="col">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th>Заявка</th>
                                    <th>Дата создания</th>
                                    <th>Действия</th>
                                </tr>
                            </thead>
                        <tbody>
                            @foreach($active_applications as $application)
                                <tr>
                                    <td>
                                        <a href="{{ route('operator.applications.show', ['application' => $application]) }}">
                                            {{$application->type->name}}
                                        </a>
                                    </td>
                                    <td>{{ $application->created_at}}</td>
                                    <td>
                                        <form
                                            action="{{ route('operator.applications.setOperator', ['application' => $application]) }}"
                                            method="post">
                                            @csrf
                                            @method('put')
                                            <button class="btn press-btn" type="submit">Начать работу</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @else
                <h3>Новых заявок пока нет</h3>
           @endif
        <h4 class="brigade-appl">Заявки переданные бригадам</h4>
        <br>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Заявка</th>
                <th>Бригада</th>
            </tr>
            </thead>
            <tbody>
            @foreach($brigade_applications as $application)
                <tr>
                    <td>{{ $application->type->name}}</td>
                    <td>{{ $application->brigade->name}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection


