@extends('layouts.operator')
@section('content')

    <div class="container mt-4">
        @include('notifications.alerts')

        <div class="row links mb-5">
            <div class="col-sm-2">
                <a href="{{ route('operator.applications.index')}}">
                    <p>Все заявки /</p>
                </a>
            </div>
            <div class="col-sm-2">
                <a href="{{ route('operator.applications.operatorApplications', ['user' => $user]) }}">
                    <p>Мои заявки /</p>
                </a>
            </div>

            <div class="col-sm-2">
                <a href="{{ route('operator.applications.closedApplications', ['user' => $user]) }}">
                    <p>Закрытые заявки /</p>
                </a>
            </div>
        </div>

        <div class="my-appl-block mt-4">
            <h3 class="mb-4">Мои заявки</h3>
            <div class="col">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Заявка</th>
                        <th>Статус</th>
                        <th>Действие</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($op_applications as $application)
                        @if($application->statusIsNotDone())
                            <tr>
                                <td>
                                    <a href="{{ route('operator.applications.show', ['application' => $application]) }}">
                                        {{$application->type->name}}
                                    </a>
                                </td>
                                <td>{{ $application->getStatusForOperator()}}</td>
                                <td>
                                    @if($application->canSetBrigade())
                                        <a class="btn btn-success press-btn" href="{{ route('operator.applications.chooseBrigade', ['application' => $application]) }}">Назначить бригаду</a>
                                    @endif

                                    @if($application->canSetDone())
                                        <form action="{{route('operator.applications.setDoneByOperator', ['application' => $application])}}" method="post">
                                            @method('put')
                                            @csrf
                                            <button class="btn btn-success press-btn" type="submit">Отметить как выполнненное</button>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
