@extends('layouts.operator')
@section('content')
    <div class="container mt-4">
        <div class="row">
            <h3>Бригады</h3>
        </div>
        <div class="mt-4">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Название</th>
                    <th>Статус</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
                @foreach($brigades as $brigade)
                    <tr>
                        <td>{{$brigade->id}}</td>
                        <td>{{$brigade->name}}</td>
                        <td>{{$brigade->status}}</td>
                        <td>
                            <form action="{{ route('operator.applications.setBrigade', ['brigade' => $brigade, 'application' => $application]) }}" method="post">
                                @method('put')
                                @csrf
                                <button class="btn btn-success press-btn" type="submit">Назначить</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection


