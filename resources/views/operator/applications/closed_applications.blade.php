@extends('layouts.operator')

@section('content')
    <div class="container mt-4">
        @include('notifications.alerts')

        <div class="row links mb-5">
            <div class="col-sm-2">
                <a href="{{ route('operator.applications.index')}}">
                    <p>Все заявки /</p>
                </a>
            </div>
            <div class="col-sm-2">
                <a href="{{ route('operator.applications.operatorApplications', ['user' => $user]) }}">
                    <p>Мои заявки /</p>
                </a>
            </div>

            <div class="col-sm-2">
                <a href="{{ route('operator.applications.closedApplications', ['user' => $user]) }}">
                    <p>Закрытые заявки /</p>
                </a>
            </div>
        </div>

        <h3>Закрытые заявки</h3>
        <table class="table table-hover mt-5">
            <thead>
            <tr>
                <th>#</th>
                <th>Заявка</th>
                <th>Бригада</th>
                <th>Статус</th>
            </tr>
            </thead>
            <tbody>
            @foreach($closed_applications as $application)
                <tr>
                    <td>{{ $application->id }}</td>
                    <td>{{ $application->type->name}}</td>
                    <td>{{ $application->brigade->name}}</td>
                    <td>{{ $application->getStatusForOperator()}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
