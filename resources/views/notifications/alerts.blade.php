
<div class="row ">
    <div class="col-md-8">
        @if (session('success'))
            <div class="alert alert-success " role="alert">
                {{session('success')}}
            </div>
        @endif
    </div>
</div>
<div class="row ">
    <div class="col-md-8">
        @if (session('error'))
            <div class="alert alert-danger " role="alert">
                {{session('error')}}
            </div>
        @endif
    </div>
</div>
<div class="row ">
    <div class="col-md-6">
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>


