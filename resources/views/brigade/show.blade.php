@extends('layouts.brigade')

@section('content')
    <div class="container mt-4">
        <h3> {{ $brigade->name }}</h3>
        <div class="row mb-5 mt-5">
            <div class="container-sm">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Заявка</th>
                        <th>Статус</th>
                        <th>Дествия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($brigade->applications as $application)
                        <tr>
                            <td>{{$application->type->name}}</td>
                            <td>{{$application-> getStatusForBrigade()}}</td>
                            <td>
                                @if($application->canSetInProcess())
                                    <form action="{{route('brigade.setInProcess', ['application' => $application])}}" method="post">
                                        @method('put')
                                        @csrf
                                        <button class="btn btn-success press-btn" type="submit">Приступить</button>
                                    </form>
                                @endif
                                @if($application->canSetClosedByBrigade())
                                    <form action="{{route('brigade.setDone', ['application' => $application])}}" method="post">
                                        @method('put')
                                        @csrf
                                        <button class="btn btn-success press-btn" type="submit">Отметить как выполненное</button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
