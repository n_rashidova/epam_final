@extends('layouts.client')
@section('content')
    <div class="container mt-4">
        <h3 class="headline mb-5">Закрытые заявки</h3>
        <table class="table table-borderless">
            <thead>
            <tr>
                <th>Заявка</th>
                <th>Комметарий</th>
                <th>Дата открытия</th>
                <th>Дата закрытия</th>
            </tr>
            </thead>
            <tbody>
                @foreach($closed_applications as $application)
                    <tr>
                        <td>{{$application->type->name}}</td>
                        <td>{{$application->comment}}</td>
                        <td>{{$application->created_at}}</td>
                        <td>{{$application->updated_at}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
