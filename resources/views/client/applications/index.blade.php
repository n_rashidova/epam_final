@extends('layouts.client')
@section('content')
    <div class="container mt-4">
        @include('notifications.alerts')
        <div class="row links">
            <div class="col-sm-2">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-success create-btn" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                    Оставить заявку
                </button>
            </div>
            <div class="col-sm-2">
                <a href="{{ route('client.applications.showClosedApplications', ['user' => $user]) }}">
                    <p>Закрытые заявки</p>
                </a>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col">
                <!-- Modal -->
                <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title " id="staticBackdropLabel"></h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form action="{{ route('client.applications.store') }}" method="post">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="application_type" class="form-label">Тип заявки</label>
                                        <select name="type_id" class="form-select  @error('type_id') is-invalid @enderror " value="{{ old('type_id') }}" aria-label="Default select example">
                                            <option selected>Выберите тип заявки из списка</option>
                                            @foreach($application_types as $type)
                                                <option value="{{$type->id}}">{{$type->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('type_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="floatingTextarea">Дополнительно</label>
                                        <textarea class="form-control @error('type_id') is-invalid @enderror " name="comment" placeholder="Комментарий" id="floatingTextarea"></textarea>

                                        @error('comment')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary create-btn" data-bs-dismiss="modal">Закрыть</button>
                                        <button type="submit" class="btn btn-success  create-btn">Отправить</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col">
                <table class="table table-borderless">
                    <thead>
                    <tr>
                        <th>Заявка</th>
                        <th>Статус</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($user->applications as $application)
                            @if($application->statusIsNotDone())
                                <tr>
                                    <td>{{$application->type->name}}</td>
                                    <td>{{$application->getStatusForClient()}}</td>
                                    <td>
                                        <form action="{{route('client.applications.setDoneByClient', ['application' => $application])}}" method="post">
                                            @method('put')
                                            @csrf
                                            <button class="btn btn-secondary change-status-btn" type="submit">Отметить как выполненное</button>
                                        </form>
                                    </td>
                                </tr>
                            @endif

                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection



