<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BrigadeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'user_id' => 'array|exists:App\Models\User,id',
            'name' => 'required|string|max:100|min:3',
            'status' => 'string'
        ];
    }
}
