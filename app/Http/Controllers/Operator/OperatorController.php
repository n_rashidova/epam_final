<?php

namespace App\Http\Controllers\Operator;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class OperatorController extends Controller
{
    /**
     * @return View|\Illuminate\Foundation\Application|Factory|Application
     */
    public function index(): View|\Illuminate\Foundation\Application|Factory|Application
    {
        $operators = User::where('role_id', 1)->get();
        return view('operator.operators.index', compact('operators'));
    }


    /**
     * @return View|\Illuminate\Foundation\Application|Factory|Application
     */
    public function create(): View|\Illuminate\Foundation\Application|Factory|Application
    {
        $staff_roles = Role::whereIn('id', [1,3])->get();
        return view('operator.operators.create', compact('staff_roles'));
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $operator = new User($request->all());
        $operator->save();
        return redirect(route('operator.users.store'))
            ->with('success', "Новый член тех поддержки успешно добавлен!");;  // написать сообщение
    }


    /**
     * @param User $user
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function edit(User $user): Factory|View|\Illuminate\Foundation\Application|Application
    {
        return view('operator.operators.edit', compact('user'));
    }


    /**
     * @param Request $request
     * @param User $user
     * @return RedirectResponse
     */
    public function update(Request $request, User $user): RedirectResponse
    {
        $user->update($request->all());
        return redirect(route('operator.users.index'))->with('success', "Информация об операторе успешно обновлена!");

    }


    /**
     * @param User $user
     * @return RedirectResponse
     */
    public function destroy(User $user): RedirectResponse
    {
        $user->delete();
        return back()->with('success', "Оператор успешно удален!");
    }


    /**
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function showBrigadeMembers(): \Illuminate\Foundation\Application|View|Factory|Application
    {
        $brigade_members = User::where('role_id', 3)->get();
        return view('operator.brigades.brigade_members', compact('brigade_members'));
    }
}
