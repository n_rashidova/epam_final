<?php

namespace App\Http\Controllers\Operator;

use App\Http\Controllers\Controller;
use App\Http\Requests\BrigadeRequest;
use App\Models\Brigade;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class BrigadeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['operator']);
    }


    /**
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function index(): \Illuminate\Foundation\Application|View|Factory|Application
    {
        $brigades = Brigade::all();
        return view('operator.brigades.index', compact('brigades' ));
    }


    /**
     * @return View|\Illuminate\Foundation\Application|Factory|Application
     */
    public function create(): View|\Illuminate\Foundation\Application|Factory|Application
    {
        $brigade_members = User::where('role_id', 3)->get();
        return view('operator.brigades.create', compact('brigade_members'));
    }


    /**
     * @param BrigadeRequest $request
     * @return RedirectResponse
     */
    public function store(BrigadeRequest $request): RedirectResponse
    {
        // надо исправить
        $brigade = new Brigade();
        $brigade->name = $request->input('name');
        $brigade->save();
        $brigade_id = $brigade->id;
        $user_ids = $request->input('user_id');
        User::whereIn('id', $user_ids)->update(['brigade_id' => $brigade_id]);

        return redirect(route('operator.brigades.index'))->with('success', 'Новая бригада успешно создана!');
    }


    /**
     * @param Brigade $brigade
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function edit(Brigade $brigade): \Illuminate\Foundation\Application|View|Factory|Application
    {
        $brigade_members = User::where('role_id', 3)->get();
        return view('operator.brigades.edit', compact('brigade', 'brigade_members'));
    }


    /**
     * @param BrigadeRequest $request
     * @param Brigade $brigade
     * @return RedirectResponse
     */
    public function update(BrigadeRequest $request, Brigade $brigade):RedirectResponse
    {
        $brigade->update($request->all());
        return redirect(route('operator.brigades.showBrigadeMembers'))
            ->with('success', "Информация о бригаде успешно обновлена!");
    }


    /**
     * @param Brigade $brigade
     * @return RedirectResponse
     */
    public function destroy(Brigade $brigade): RedirectResponse
    {
        $brigade->delete();
        return back();
    }
}
