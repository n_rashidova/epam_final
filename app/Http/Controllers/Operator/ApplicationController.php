<?php

namespace App\Http\Controllers\Operator;

use App\Http\Controllers\Controller;
use App\Models\Application;
use App\Models\Brigade;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class ApplicationController extends Controller
{
    public function __construct()
    {
        $this->middleware(['operator']);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function index(): \Illuminate\Foundation\Application|View|Factory|\Illuminate\Contracts\Foundation\Application
    {
        $applications = Application::all();
        $brigades = Brigade::all();
        $user = Auth::user();
        $brigade_applications = Application::where('status',Application::TAKEN_BY_TEAM)
            ->get();

        $active_applications = Application::where(
            'status',
            Application::ACTIVE)
            ->get();

        $op_applications = Application::where(
            'operator_id',
            Auth::id()
        )->get();

        return view('operator.applications.index',
            compact('user',
                'applications',
                'active_applications',
                'op_applications',
                'brigades',
                'brigade_applications'
            )
        );
    }


    /**
     * @param User $user
     * @return View|\Illuminate\Foundation\Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function closedApplications(User $user): View|\Illuminate\Foundation\Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        $closed_applications = Application::where('operator_id', $user->id)
            ->where('status', Application::DONE)->get();
        return view('operator.applications.closed_applications', compact('user', 'closed_applications' ));
    }


    /**
     * @param Application $application
     * @return View|\Illuminate\Foundation\Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function show(Application $application): View|\Illuminate\Foundation\Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        $user = $application->user;
        return view('operator.applications.show', compact('application', 'user'));
    }


    /**
     * @param User $user
     * @return View|\Illuminate\Foundation\Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function operatorApplications(User $user): View|\Illuminate\Foundation\Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        $op_applications = Application::where('operator_id', Auth::id())->get();
        return view('operator.applications.op_applications', compact('op_applications', 'user'));
    }


    /**
     * @param Application $application
     * @return View|\Illuminate\Foundation\Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function chooseBrigade(Application $application): View|\Illuminate\Foundation\Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        $brigades = Brigade::all();
        return view('operator.applications.brigades', compact('brigades', 'application'))
            ->with('Бригада назначена!');
    }


    /**
     * @param Application $application
     * @return RedirectResponse
     */
    public function setOperator(Application $application): RedirectResponse
    {
        $user = Auth::user();
        $application->status = Application::TAKEN_BY_OPERATOR;
        $application->operator_id = $user->id;
        $application->update();
        return redirect(route('operator.applications.operatorApplications', compact('user')))
            ->with('success', 'Заявка у оператора!');
    }


    /**
     * @param Application $application
     * @param Brigade $brigade
     * @return RedirectResponse
     */
    public function setBrigade( Application $application, Brigade $brigade):RedirectResponse
    {
        $application->status = Application::TAKEN_BY_TEAM;
        $application->brigade_id = $brigade->id;
        $application->update();
        return redirect(route('operator.applications.index'))
            ->with('success', "Заявка передана бригаде $brigade->name!");
    }

    /**
     * @param Application $application
     * @return RedirectResponse
     */
    public function setDoneByOperator(Application $application): RedirectResponse
    {
        if($application->status === Application::CLOSED_BY_CLIENT) {
            $application->status = Application::DONE;
            $application->update();
            return back()
                ->with('success',
                    'Заявка выполнена!');
        } else {
            return back()
                ->with('error',
                    'Ещё не вся работа сделана, пожалуйста отметьте эту задачу когда бригада и клиент отметят её как выполненную!');
        }
    }
}
