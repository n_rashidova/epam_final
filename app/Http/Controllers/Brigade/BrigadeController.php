<?php

namespace App\Http\Controllers\Brigade;

use App\Http\Controllers\Controller;
use App\Models\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class BrigadeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['brigade']);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function show(): Factory|View|\Illuminate\Foundation\Application|\Illuminate\Contracts\Foundation\Application
    {
        $user = Auth::user();
        $brigade = $user->brigade;
        return view('brigade.show', compact( 'brigade', 'user'));
    }


    /**
     * @param Application $application
     * @return RedirectResponse
     */
    public function setInProcess(Application $application): RedirectResponse
    {
        $application->status = Application::IN_PROGRESS;
        $application->update();
        return back();
    }

    /**
     * @param Application $application
     * @return RedirectResponse
     */
    public function setDone(Application $application): RedirectResponse
    {
        $application->status = Application::CLOSED_BY_BRIGADE;
        $application->update();
        return back();
    }
}
