<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\ApplicationRequest;
use App\Models\Type;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApplicationController extends Controller
{
    public function __construct()
    {
        $this->middleware(['client']);
    }

    /**
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     * @throws AuthorizationException
     */
    public function index(): Factory|View|\Illuminate\Foundation\Application|Application
    {

        $user = Auth::user();
        $application_types = Type::all();
        $this->authorize('viewAny', \App\Models\Application::class);
        return view('client.applications.index',
            compact('application_types', 'user')
        );
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(ApplicationRequest $request): RedirectResponse
    {
        $application = new \App\Models\Application($request->all());
        $application->user()->associate($request->user());
        $application->save();
        return back()->with('success','Новая заявка успешно создана!');
    }


    /**
     * @param User $user
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function showClosedApplications(User $user): Factory|View|\Illuminate\Foundation\Application|Application
    {
        $closed_applications = $user->applications->where('status', \App\Models\Application::DONE);
        return view('client.applications.closed_applications', compact('closed_applications'));
    }


    /**
     * @param \App\Models\Application $application
     * @return RedirectResponse
     */
    public function setDoneByClient(\App\Models\Application $application): RedirectResponse
    {
        if ($application->status === \App\Models\Application::CLOSED_BY_BRIGADE){
            $application->status = \App\Models\Application::CLOSED_BY_CLIENT;
        }

        if(!!$application->update()){
            return back()->with('success', "Ваша заявка успешно выполнена!");
        } else {
            return back()->with('error', "Ваша заявка еще не выполнена!");
        }

    }
}
