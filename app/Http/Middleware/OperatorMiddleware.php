<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class OperatorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(Request): (Response) $next
     * @return Application|\Illuminate\Foundation\Application|RedirectResponse|Redirector|mixed|Response|void
     */
    public function handle(Request $request, Closure $next)
    {
        if(!Auth::check()){
            return redirect('/login');
        }

        $user = Auth::user();
        if($user->isOperator()){
            return $next($request);

        } elseif($user->isBrigadeMember())
        {
            $brigade = $user->brigade;
            return redirect(route('brigade.show',['brigade'=> $brigade]));
        } elseif ($user->isClient())
        {
            return redirect('/client/applications');
        }
    }
}
