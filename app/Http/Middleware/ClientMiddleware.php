<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class ClientMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(Request): (Response) $next
     */

    public function handle(Request $request, Closure $next)
    {
        if(!Auth::check()){
            return redirect('/login');
        }

        $user = Auth::user();
        if($user->isClient()){
            return $next($request);

        } elseif ($user->isOperator()){
            return redirect('/operator/applications');

        } elseif($user->isBrigadeMember()){
            $brigade = $user->brigade;
            return redirect(route('brigade.show',['brigade'=> $brigade]));
        }
    }
}
