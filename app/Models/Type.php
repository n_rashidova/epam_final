<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Type extends Model
{
    use HasFactory;

    protected $fillable = ['name'];


    public const INSTALL_ECO_BOX  = 'Установить экобокс';
    public const TAKE_TRASH  = 'Вывезти мусор';
    public const FIX_ECO_BOX  = 'Демонтировать экобокс';

    public const TYPES = [self::FIX_ECO_BOX, self::INSTALL_ECO_BOX, self::TAKE_TRASH];
    /**
     * @return HasMany
     */
    public function applications(): HasMany
    {
        return $this->hasMany(Application::class);
    }
}
