<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Application extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'type_id',
        'comment',
        'status',
        'operator_id',
        'brigade_id',
    ];

    public const ACTIVE = 'active';
    public const TAKEN_BY_OPERATOR = 'taken by operator';
    public const TAKEN_BY_TEAM = 'taken by team';
    public const IN_PROGRESS = 'in_progress';
    public const CLOSED_BY_BRIGADE = 'closed by brigade';
    public const CLOSED_BY_CLIENT = 'closed by client';
    public const DONE = 'closed';


    public const STATUSES = [
        self::ACTIVE,
        self::IN_PROGRESS,
        self::DONE,
    ];


    // Методы клиента

    /**
     * @return string|void
     */

    public function statusIsNotDone()
    {
        return $this->status !== Application::DONE;
    }


    /**
     * @return string
     */
    public function getStatusForClient(): string
    {
        return match ($this->status) {
            self::IN_PROGRESS => 'В процессе',
            self::DONE, self::CLOSED_BY_CLIENT => 'Выполнена',
            default => 'Активная',
        };
    }


    /**
     * @return bool
     */
    public function canSetClosedByClient(): bool
    {

        return $this->status === Application::CLOSED_BY_BRIGADE;
    }




    // Методы оператора
    /**
     * @return bool
     */
    public function canSetBrigade():bool
    {
        return $this->status === Application::TAKEN_BY_OPERATOR;
    }


    public function canSetDone()
    {
        if($this->status === self::TAKEN_BY_TEAM or
            $this->status == self::CLOSED_BY_CLIENT or
            $this->status == self::CLOSED_BY_BRIGADE )
        {
            return true;
        }
    }


    public function getStatusForOperator():string
    {
        return match ($this->status) {
            self::IN_PROGRESS => 'В процессе',
            self::DONE => 'Выполнена',
            self::TAKEN_BY_TEAM => 'У бригады',
            self::CLOSED_BY_BRIGADE => 'Выполнено бригадой',
            self::CLOSED_BY_CLIENT => 'Отмечено клиентом',
            default => 'Новая',
        };
    }




    // Методы бригады
    /**
     * @return bool
     */
    public function canSetInProcess():bool
    {
        return $this->status === Application::TAKEN_BY_TEAM;
    }


    /**
     * @return bool
     */
    public function canSetClosedByBrigade(): bool
    {
        return $this->status === Application::IN_PROGRESS;
    }

    public function getStatusForBrigade()
    {
        return match ($this->status) {
            self::IN_PROGRESS => 'В процессе',
            self::CLOSED_BY_BRIGADE, self::CLOSED_BY_CLIENT, self::DONE => 'Выполнено',
            default => 'Активная',
        };
    }


    /**
     * @return BelongsTo
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(Type::class);
    }


    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }


    /**
     * @return BelongsTo
     */
    public function brigade(): BelongsTo
    {
        return $this->belongsTo(Brigade::class);
    }

}
