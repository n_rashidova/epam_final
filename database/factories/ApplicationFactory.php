<?php

namespace Database\Factories;

use App\Models\Application;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Application>
 */
class ApplicationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'user_id' => rand(1,10),
            'type_id' => rand(1,3),
            'comment' => fake()->sentence(),
            'status' => fake()->randomElement(Application::STATUSES),
            'operator_id' => 0,
//            'brigade_id' => rand(1,3),
        ];
    }
}
