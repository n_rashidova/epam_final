<?php

namespace Database\Seeders;

use App\Models\Brigade;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BrigadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Brigade::factory()->count(3)->create();
    }
}
